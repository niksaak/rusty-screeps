use std::{
    collections::{HashMap, HashSet},
};

use screeps::{
    prelude::*, game, Attackable,
    objects::{Room, Structure, Creep},
    constants::{Part, ReturnCode, ResourceType, find},
};

use tasks::{self, Task, ExecuteStatus};
use memory::Memory;
use creep::{CreepName, CreepKind, CreepState};

pub struct RoomState {
    pub memory: Memory<RoomMemory>,
    pub creeps: HashMap<CreepName, CreepState>,
    pub aliens: HashMap<String, Creep>,
    pub idle: HashSet<CreepName>,
}

impl RoomState {
    pub fn new(room: &Room) -> Self {
        let mut room_state = Self {
            memory: Memory::from_reference(room.memory()),
            creeps: HashMap::new(),
            aliens: HashMap::new(),
            idle: HashSet::new(),
        };

        // Establish my creeps in this room.
        for creep in room.find(find::MY_CREEPS) {
            let creep_name = creep.name().parse().unwrap();
            let creep_state = CreepState::new(creep, creep_name);

            room_state.creeps.insert(creep_name, creep_state);
            room_state.idle.insert(creep_name);
        }

        // Establish enemy creeps in this room.
        for alien in room.find(find::HOSTILE_CREEPS) {
            room_state.aliens.insert(alien.name(), alien);
        }

        // Update transient state for tasks.
        {
            let tasks = &mut room_state.memory.fetch_mut_or_default().unwrap().tasks;
            for task in tasks.values_mut() {
                let mut unassign = vec![];
                for name in &task.assignees {
                    room_state.idle.remove(name);
                    if room_state.creeps.get(name).is_none() {
                        unassign.push(name.clone());
                    }
                }
                task.assignees.retain(|a| unassign.iter().find(|u| &a == u).is_none());
            }
        }

        room_state
    }
}

#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize, Default)]
pub struct RoomMemory {
    tasks: HashMap<String, Task>,
}

pub struct RoomExt<'r, 's> {
    pub room: &'r Room,
    pub state: &'s mut RoomState,
}

impl<'r, 's> RoomExt<'r, 's> {
    pub fn new(room: &'r Room, state: &'s mut RoomState) -> Self {
        Self { room, state }
    }

    pub fn process(&mut self) -> Result<(), ()> {
        let _ = self.manage_spawn();
        self.manage_tasks();
        Ok(())
    }

    fn manage_spawn(&self) -> Result<(), ()> {
        trace!("manage_spawn");
        const DRONE_BODY: [Part; 4] = [Part::Work, Part::Work, Part::Carry, Part::Move];
        const HARVESTER_BODY: [Part; 6] = [
            Part::Work, Part::Work, Part::Work, Part::Work,
            Part::Carry,
            Part::Move,
        ];

        let spawn = self.room.find(find::MY_SPAWNS)
            .into_iter()
            .next()
            .ok_or(())?;

        if spawn.is_spawning() || self.room.energy_available() < 300 {
            return Ok(());
        }

        trace!("manage_spawn.drone");

        let room_name = self.room.name_local();

        let drone_count_max = 9;
        let drone_count = self.state.creeps.keys().filter(|c| c.kind == CreepKind::Drone).count();

        if drone_count < drone_count_max {
            for id in 0.. {
                trace!("drone: loop {}", id);
                let name = CreepName::new(room_name, CreepKind::Drone, id);
                let result = spawn.spawn_creep(&DRONE_BODY, &name.to_string());
                match result {
                    ReturnCode::NameExists => continue,
                    ReturnCode::Ok => {
                        debug!("Spawning {}", &name);
                        break;
                    }
                    code => {
                        trace!("result code: {:?}", code);
                        break;
                    }
                }
            }
        }

        trace!("manage_spawn.harvester");

        let harvester_count_max = 4;
        let harvester_count = self.state.creeps.keys().filter(|c| c.kind == CreepKind::Harvester).count();

        if harvester_count < harvester_count_max {
            for id in 0.. {
                trace!("drone: loop {}", id);
                let name = CreepName::new(room_name, CreepKind::Harvester, id);
                let result = spawn.spawn_creep(&HARVESTER_BODY, &name.to_string());
                match result {
                    ReturnCode::NameExists => continue,
                    ReturnCode::Ok => {
                        debug!("Spawning {}", &name);
                        break;
                    }
                    code => {
                        trace!("result code: {:?}", code);
                        break;
                    }
                }
            }
        }

        Ok(())
    }

    fn manage_tasks(&mut self) {
        trace!("manage_tasks");

        let mut memory = self.state.memory.fetch_mut_or_default().unwrap();

        // Collect tasks.
        if game::time() % 10 == 0 {
            trace!("manage_tasks.collect");
            let tasks = &mut memory.tasks;

            let has_harvesters = self.state.creeps.keys()
                .position(|c| c.kind.is_specialist())
                .is_some();

            {
                let sources = self.room.find(find::SOURCES);
                for source in sources {
                    if !has_harvesters {
                        if source.energy() == 0 {
                            tasks.remove(&source.id());
                            continue;
                        }

                        tasks.entry(source.id())
                            .and_modify(|t| {
                                t.kind = tasks::Kind::Harvest;
                                t.capacity = t.kind.default_capacity();
                            })
                            .or_insert(tasks::Kind::Harvest.into());
                    } else {
                        tasks.entry(source.id())
                            .and_modify(|t| {
                                t.kind = tasks::Kind::StaticHarvest;
                                t.capacity = t.kind.default_capacity();
                            })
                            .or_insert(tasks::Kind::StaticHarvest.into());
                    }
                }
            }

            {
                let structures = self.room.find(find::STRUCTURES);
                for struc in &structures {
                    match struc {
                        Structure::Spawn(spawn) 
                            if spawn.energy() < spawn.energy_capacity() =>
                        {
                            tasks.entry(spawn.id()).or_insert(tasks::Kind::Feed.into());
                        }
                        Structure::Extension(ext)
                            if ext.energy() < ext.energy_capacity() =>
                        {
                            tasks.entry(ext.id()).or_insert(tasks::Kind::Feed.into());
                        }
                        Structure::Tower(tower)
                            if tower.energy() < tower.energy_capacity() =>
                        {
                            tasks.entry(tower.id()).or_insert(tasks::Kind::Feed.into());
                        }
                        _ => (),
                    }
                }
            }

            {
                let controller = self.room.controller()
                    .expect("Room has no controller.");
                tasks.entry(controller.id()).or_insert(tasks::Kind::Upgrade.into());
            }

            {
                let sites = self.room.find(find::CONSTRUCTION_SITES);
                for site in sites {
                    tasks.entry(site.id()).or_insert(tasks::Kind::Build.into());
                }
            }

            {
                let mut structures = self.room.find(find::STRUCTURES);
                structures.retain(|structure| {
                    match structure {
                        Structure::Controller(_) => false,
                        Structure::KeeperLair(_) => false,
                        Structure::Container(c) => {
                            let hits_ratio = structure.hits() as f32 / structure.hits_max() as f32;
                            trace!("Container {} hits ratio: {}", c.id(), hits_ratio);
                            hits_ratio < 0.4_f32
                        }
                        _ => (structure.hits() as f32 / structure.hits_max() as f32) < 0.4_f32,
                    }
                });
                for structure in structures {
                    tasks.entry(structure.id()).or_insert_with(||{
                        debug!("New task: repair {}", structure.id());
                        tasks::Kind::Repair.into()
                    });
                }
            }

            {
                let mut resources = self.room.find(find::DROPPED_RESOUCES);
                resources.retain(|r| r.resource_type() == ResourceType::Energy);
                for resource in resources {
                    tasks.entry(resource.id()).or_insert(tasks::Kind::Gather.into());
                }
            }

            {
                let containers = self.room.find(find::STRUCTURES)
                    .into_iter()
                    .filter_map(|structure| {
                        match structure {
                            Structure::Container(c) => Some(c),
                            _ => None
                        }
                    })
                    .collect::<Vec<_>>();

                for container in containers {
                    tasks.entry(container.id()).or_insert(tasks::Kind::Resupply.into());
                }
            }
        }

        // Assign tasks.
        if self.state.idle.len() > 0 {
            trace!("manage_tasks.assign");
            debug!("Idlers: {}", self.state.idle.len());

            let tasks = &mut memory.tasks;
            tasks.retain(|target, _| {
                game::get_object_erased(target).is_some()
            });

            let mut tasks = tasks.iter_mut().collect::<Vec<_>>();

            for idler in self.state.idle.drain() {
                let state = self.state.creeps.get(&idler).expect("idle creep doesn't exist");
                let task = tasks.iter_mut()
                    .filter(|(_, t)| idler.kind.can_do(t.kind))
                    .filter(|(_, t)| t.can_assign(&state))
                    .filter(|(_, t)| t.effective_capacity() > 0)
                    .map(|(target, task)| {
                        let object = game::get_object_erased(target).unwrap();
                        (target, task, object)
                    })
                    .min_by(|(_, a_task, a), (_, b_task, b)| {
                        let a_range = a.pos().get_range_to(&state.creep);
                        let b_range = b.pos().get_range_to(&state.creep);
                        a_range.cmp(&b_range)
                            .then(a_task.effective_capacity().cmp(&b_task.effective_capacity()).reverse())
                    });

                if let Some((target, task, _)) = task {
                    info!("Assigning {} to {:?} @ {}", idler, task.kind, target);
                    task.assignees.push(idler);
                }
            }
        }

        // Execute tasks.
        {
            trace!("manage_tasks.execute");
            let tasks = &mut memory.tasks;
            let mut finished = Vec::new();

            debug!("Task count: {}", tasks.len());

            for (target, task) in tasks.iter_mut() {
                if game::get_object_erased(target).is_none() {
                    finished.push(target.clone());
                    continue;
                }

                for assignee in task.assignees.clone().iter() {
                    let creep_state = self.state.creeps.get(assignee);

                    if let Some(state) = creep_state {
                        if !task.can_assign(state) {
                            task.assignees.retain(|a| a != assignee);
                            continue;
                        }

                        if task.execute(state, target) == ExecuteStatus::Finish {
                            info!("Completed: {:?} @ {}", task.kind, target);
                            finished.push(target.clone());
                        }
                    }
                }
            }

            for target in finished {
                tasks.remove(&target);
            }
        }

        // Manage turrets
        {
            trace!("manage_tasks.turrets");
            let mut turrets = self.room.find(find::STRUCTURES)
                .into_iter()
                .filter_map(|s| match s { Structure::Tower(t) => Some(t), _ => None });

            if self.state.aliens.len() > 0 {
                let attacks = self.state.aliens.iter().zip(&mut turrets);
                for ((name, alien), turret) in attacks {
                    warn!("Tower {} attacks alien {} from {}",
                          turret.id(), name, alien.owner_name());
                    turret.attack(&alien);
                }
            }

            {
                let unhealthy = self.state.creeps.iter()
                    .filter(|(_, state)| state.creep.hits() < state.creep.hits_max());
                let heals = unhealthy.zip(&mut turrets);
                for ((name, state), turret) in heals {
                    info!("Tower {} heals {}", turret.id(), name);
                    turret.heal(&state.creep);
                }
            }

            {
                let tasks = memory.tasks.iter()
                    .filter(|(_, t)| t.kind == tasks::Kind::Repair)
                    .filter(|(target, _)| {
                        let structure = game::get_object_typed::<Structure>(&target).unwrap().unwrap();
                        (structure.hits() as f32 / structure.hits_max() as f32) < 0.3
                            && structure.hits() < 1000
                    });

                let repairs = tasks.zip(&mut turrets);
                for ((target, _), turret) in repairs {
                    let structure = game::get_object_typed::<Structure>(&target).unwrap().unwrap();
                    info!("Tower {} repairs {}", turret.id(), &target);
                    turret.repair(&structure);
                }
            }
        }
    }
}
