use std::str;
use std::cell::{RefCell, Ref, RefMut};

use screeps::memory::MemoryReference;
use bincode;
use serde::{Serialize, de::DeserializeOwned};

use base122;

pub struct Memory<T: Serialize>(RefCell<MemoryInner<T>>);

pub fn pack<T: Serialize>(data: T) -> Result<String, bincode::Error> {
    let serialized = bincode::serialize(&data)?;
    let stringified = base122::encode(&serialized);
    Ok(String::from_utf8(stringified).expect("Base122 encoded string is not valid utf-8"))
}

pub fn unpack<T: DeserializeOwned>(string: &str) -> Result<T, bincode::Error> {
    let raw = base122::decode(string.as_bytes());
    let data = bincode::deserialize(&raw)?;
    Ok(data)
}

enum MemoryInner<T> {
    Referenced(Option<MemoryReference>),
    Cached(MemoryData<T>),
}

struct MemoryData<T> {
    data: Option<T>,
    reference: Option<MemoryReference>,
    dirty: bool,
}

impl<T: DeserializeOwned + Serialize> Memory<T> {
    pub fn new(data: T) -> Self {
        Memory(RefCell::new(MemoryInner::Cached(
            MemoryData {
                data: Some(data),
                reference: Some(MemoryReference::new()),
                dirty: true,
            }
        )))
    }

    pub fn from_reference(mr: MemoryReference) -> Self {
        Memory(RefCell::new(MemoryInner::Referenced(Some(mr))))
    }

    fn fetch_if_referenced(&self) -> Result<(), bincode::Error> {
        let mut inner = self.0.borrow_mut();
        if let MemoryInner::Referenced(reference) = &mut *inner {
            let data = reference
                .as_ref().unwrap()
                .string("data").unwrap()
                .map(|string| unpack(&string).expect("error while fetching memory"));
            *inner = MemoryInner::Cached(MemoryData {
                data,
                reference: reference.take(),
                dirty: false,
            });
        }
        Ok(())
    }

    pub fn fetch_opt(&self) -> Result<Ref<Option<T>>, bincode::Error> {
        self.fetch_if_referenced()?;
        Ok(
            Ref::map(self.0.borrow(), |inner| {
                match &inner {
                    MemoryInner::Cached(inner) => &inner.data,
                    _ => unreachable!(),
                }
            }
        ))
    }

    pub fn fetch_mut_opt(&self) -> Result<RefMut<Option<T>>, bincode::Error> {
        self.fetch_if_referenced()?;
        Ok(RefMut::map(self.0.borrow_mut(), |inner| {
            match inner {
                MemoryInner::Cached(inner) => {
                    inner.dirty = true;
                    &mut inner.data
                }
                _ => unreachable!(),
            }
        }))
    }

    pub fn fetch(&self) -> Result<Ref<T>, bincode::Error> {
        self.fetch_opt().map(|rf| {
            Ref::map(rf, |opt| opt.as_ref().expect("fetch failed"))
        })
    }

    pub fn fetch_mut(&self) -> Result<RefMut<T>, bincode::Error> {
        self.fetch_mut_opt().map(|rf| {
            RefMut::map(rf, |opt| opt.as_mut().expect("fetch failed"))
        })
    }
}

impl<T: DeserializeOwned + Serialize + Default> Memory<T> {
    pub fn fetch_or_default(&self) -> Result<Ref<T>, bincode::Error> {
        self.fetch_mut_or_default()?;
        self.fetch()
    }

    pub fn fetch_mut_or_default(&self) -> Result<RefMut<T>, bincode::Error> {
        self.fetch_mut_opt().map(|rf| {
            RefMut::map(rf, |opt| {
                opt.get_or_insert_with(Default::default)
            })
        })
    }
}

impl<T: Serialize> Memory<T> {
    pub fn commit(&mut self) -> Result<(), bincode::Error> {
        let mut inner = self.0.borrow_mut();
        match &mut *inner {
            MemoryInner::Cached(MemoryData{ data: Some(data), reference, dirty }) if *dirty => {
                reference
                    .as_ref().unwrap()
                    .set(
                        "data",
                        pack(data)?,
                    );
                *inner = MemoryInner::Referenced(reference.take());
            }
            MemoryInner::Cached(MemoryData { reference, dirty, .. }) if *dirty => {
                reference
                    .as_ref().unwrap()
                    .del("data");
                *inner = MemoryInner::Referenced(reference.take());
            }
            _ => (),
        }
        Ok(())
    }

    pub fn into_inner(mut self) -> Result<MemoryReference, bincode::Error> {
        self.commit()?;
        match &mut *self.0.borrow_mut() {
            MemoryInner::Referenced(memory) => Ok(memory.take().unwrap()),
            _ => unreachable!(),
        }
    }
}

impl<T: Serialize> Drop for Memory<T> {
    fn drop(&mut self) {
        if let MemoryInner::Referenced(None) = &mut *self.0.borrow_mut() {
            return;
        }
        self.commit().expect("committing in drop unsuccessfull.");
    }
}

