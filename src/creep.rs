use std::{
    fmt,
    error::Error,
    str::FromStr,
};

use screeps::{
    LocalRoomName,
    objects::Creep,
};

use tasks;

pub struct CreepState {
    pub creep: Creep,
    pub name: CreepName,
}

impl CreepState {
    pub fn new(creep: Creep, name: CreepName) -> Self {
        CreepState { creep, name }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct CreepName {
    pub room: LocalRoomName,
    pub kind: CreepKind,
    pub id: u32,
}

impl CreepName {
    pub fn new(room: LocalRoomName, kind: CreepKind, id: u32) -> Self {
        CreepName { room, kind, id }
    }
}

impl fmt::Display for CreepName {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}-{:?}-{}", self.room, self.kind, self.id)
    }
}

impl FromStr for CreepName {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts = s.split('-').collect::<Vec<&str>>();
        if parts.len() != 3 {
            return Err(ParseError::Name);
        }
        let room = LocalRoomName::new(parts[0]).map_err(|_| ParseError::Name)?;
        let kind = parts[1].parse()?;
        let id = parts[2].parse().map_err(|_| ParseError::Name)?;
        Ok(Self { room, kind, id })
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum CreepKind {
    Harvester,
    Drone,
}

impl CreepKind {
    pub fn can_do(&self, kind: tasks::Kind) -> bool {
        match (self, kind) {
            (CreepKind::Harvester, tasks::Kind::StaticHarvest) => true,
            (_, tasks::Kind::StaticHarvest) => false,
            (CreepKind::Harvester, _) => false,
            (CreepKind::Drone, _) => true,
        }
    }

    pub fn is_specialist(&self) -> bool {
        self != &CreepKind::Drone
    }
}

impl FromStr for CreepKind {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use self::CreepKind::*;

        match s {
            "Harvester" => Ok(Harvester),
            "Drone" => Ok(Drone),
            _ => Err(ParseError::Name),
        }
    }
}

#[derive(Debug, Clone)]
pub enum ParseError {
    Name,
    Kind,
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::ParseError::*;

        match self {
            Name => write!(f, "malformed creep name"),
            Kind => write!(f, "malformed creep kind"),
        }
    }
}

impl Error for ParseError {}

