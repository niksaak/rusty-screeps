#![feature(nll)]
#![allow(dead_code)]
#![recursion_limit = "128"]

extern crate fern;
#[macro_use]
extern crate log;
extern crate screeps;
#[macro_use]
extern crate stdweb;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate bincode;

mod logging;
mod state;
mod room;
mod tasks;
mod base122;
mod memory;
mod creep;

use state::State;
use room::RoomExt;

fn main() {
    stdweb::initialize();
    logging::setup_logging(logging::Info);

    js! {
        var game_loop = @{game_loop};

        module.exports.loop = function() {
            // Provide actual error traces.
            try {
                game_loop();
            } catch (error) {
                // console_error function provided by 'screeps-game-api'
                console_error("caught exception:", error);
                if (error.stack) {
                    console_error("stack trace:", error.stack);
                }
                console_error("resetting VM next tick.");
                // reset the VM since we don't know if everything was cleaned up and don't
                // want an inconsistent state.
                module.exports.loop = function() {
                    __initialize(new WebAssembly.Module(require("compiled")), false);
                    module.exports.loop();
                }
            }
        }
    }
}

fn game_loop() {
    let mut state = State::new();

    for room in screeps::game::rooms::values() {
        debug!("Processing room {}", &room.name_local());
        let result = RoomExt::new(&room, state.rooms.get_mut(&room.name_local()).unwrap())
            .process();
        if let Err(e) = result {
            error!("Failed to process room {}: {:?}", &room.name(), e);
        }
    }
}

