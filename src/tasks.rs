use screeps::{
    prelude::*, Attackable, HasStore, game,
    objects::{
        Source, StructureController, StructureContainer, ConstructionSite,
        Structure, Resource,
    },
    constants::{ResourceType, ReturnCode, look},
};

use creep::{CreepState, CreepName};

#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum Kind {
    // T0
    Harvest,
    Feed,
    Upgrade,
    Build,
    Repair,
    Gather,
    Resupply,

    // T1
    StaticHarvest,
}

impl Kind {
    pub fn default_capacity(&self) -> u32 {
        match self {
            // T0
            Kind::Harvest => 2,
            Kind::Feed => 1,
            Kind::Upgrade => 5,
            Kind::Build => 4,
            Kind::Repair => 1,
            Kind::Gather => 8,
            Kind::Resupply => 8,

            // T1
            Kind::StaticHarvest => 2,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ExecuteStatus {
    Continue,
    Finish,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Task {
    pub kind: Kind,
    pub assignees: Vec<CreepName>,
    pub capacity: u32,
}

impl Task {
    pub fn execute(
        &mut self,
        state: &CreepState,
        target: &str,
    ) -> ExecuteStatus {
        let creep = &state.creep;
        match self.kind {
            Kind::Harvest => {
                let target = game::get_object_typed::<Source>(target)
                    .expect("Harvest target is not a source")
                    .unwrap();
                if creep.harvest(&target) == ReturnCode::NotInRange {
                    creep.move_to(&target);
                }
                if target.energy() == 0 {
                    return ExecuteStatus::Finish;
                }
            }
            Kind::Feed => {
                let target = game::get_object_typed::<Structure>(target)
                    .expect("Feed target is not a spawn")
                    .unwrap();
                match &target {
                    Structure::Spawn(target) => {
                        if creep.transfer_all(target, ResourceType::Energy) == ReturnCode::NotInRange {
                            creep.move_to(target);
                        }
                        if target.energy() == target.energy_capacity() {
                            return ExecuteStatus::Finish;
                        }
                    }
                    Structure::Extension(target) => {
                        if creep.transfer_all(target, ResourceType::Energy) == ReturnCode::NotInRange {
                            creep.move_to(target);
                        }
                        if target.energy() == target.energy_capacity() {
                            return ExecuteStatus::Finish;
                        }
                    }
                    Structure::Tower(target) => {
                        if creep.transfer_all(target, ResourceType::Energy) == ReturnCode::NotInRange {
                            creep.move_to(target);
                        }
                        if target.energy() == target.energy_capacity() {
                            return ExecuteStatus::Finish;
                        }
                    }
                    _ => {
                        error!("Structure is not a spawn or extension: {}", target.id());
                        return ExecuteStatus::Finish;
                    }
                }
            }
            Kind::Upgrade => {
                let target = game::get_object_typed::<StructureController>(target)
                    .expect("Find target is not a controller")
                    .unwrap();
                if creep.upgrade_controller(&target) == ReturnCode::NotInRange {
                    creep.move_to(&target);
                }
            }
            Kind::Build => {
                let target = game::get_object_typed::<ConstructionSite>(target)
                    .expect("Build target is not a construction site");
                if let Some(target) = target {
                    if creep.build(&target) == ReturnCode::NotInRange {
                        creep.move_to(&target);
                    }
                } else {
                    return ExecuteStatus::Finish;
                }
            }
            Kind::Repair => {
                let target = game::get_object_typed::<Structure>(target)
                    .expect("Repair target is not a structure")
                    .unwrap();

                if creep.repair(&target) == ReturnCode::NotInRange {
                    creep.move_to(&target);
                }
                if target.hits() == target.hits_max() {
                    return ExecuteStatus::Finish;
                }
            }
            Kind::Gather => {
                let resource_opt = game::get_object_typed::<Resource>(target)
                    .expect("Gather target is not a resource");

                if let Some(resource) = &resource_opt {
                    if creep.pickup(resource) == ReturnCode::NotInRange {
                        creep.move_to(resource);
                    }
                }

                let resource_opt = resource_opt.and_then(|_| {
                    game::get_object_erased(target)
                });

                if resource_opt.is_none() {
                    return ExecuteStatus::Finish;
                }
            }
            Kind::Resupply => {
                let target = game::get_object_typed::<StructureContainer>(target)
                    .expect("Resupply target is not a container")
                    .unwrap();
                
                if creep.withdraw_all(&target, ResourceType::Energy) == ReturnCode::NotInRange {
                    creep.move_to(&target);
                }

                if target.store_of(ResourceType::Energy) == 0 {
                    return ExecuteStatus::Finish;
                }
            }

            Kind::StaticHarvest => {
                let target = game::get_object_typed::<Source>(target)
                    .expect("Harvest target is not a source")
                    .unwrap();

                if creep.harvest(&target) == ReturnCode::NotInRange {
                    creep.move_to(&target);
                    return ExecuteStatus::Continue;
                }

                if creep.energy() == creep.carry_capacity() {
                    let pos = creep.pos();
                    let (x, y) = (pos.x() as u8, pos.y() as u8);
                    let container = creep.room()
                        .look_for_at_area(look::STRUCTURES, x-1..x+2, y-1..y+2)
                        .into_iter()
                        .filter_map(|s| match s { Structure::Container(c) => Some(c), _ => None })
                        .next();
                    if let Some(c) = container {
                        creep.transfer_all(&c, ResourceType::Energy);
                    } else {
                        creep.drop(ResourceType::Energy, None);
                    }
                }
            }
        }
        ExecuteStatus::Continue
    }

    pub fn can_assign(&self, creep_state: &CreepState) -> bool {
        use self::Kind::*;

        let creep = &creep_state.creep;
        match self.kind {
            StaticHarvest => true,
            Harvest | Gather => creep.energy() < creep.carry_capacity(),
            Resupply => creep.energy() == 0,
            Feed | Upgrade | Build | Repair => creep.energy() > 0,
        }
    }

    pub fn effective_capacity(&self) -> u32 {
        self.capacity - self.assignees.len() as u32
    }
}

impl From<Kind> for Task {
    fn from(kind: Kind) -> Self {
        Self { kind, assignees: vec![], capacity: kind.default_capacity() }
    }
}

