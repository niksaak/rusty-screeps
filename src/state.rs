use std::collections::HashMap;
use room::RoomState;

use screeps::{
    game,
    LocalRoomName,
};

pub struct State {
    pub rooms: HashMap<LocalRoomName, RoomState>,
}

impl State {
    pub fn new() -> Self {
        let mut rooms = HashMap::<_, RoomState>::new();

        for room in game::rooms::values() {
            rooms.entry(room.name_local()).or_insert(RoomState::new(&room));
        }

        State { rooms }
    }
}

